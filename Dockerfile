FROM node:18-alpine as build

WORKDIR /app

RUN apk add python3 build-base

COPY ./ ./

RUN yarn install

RUN yarn build

RUN yarn install --production

FROM node:18-alpine

WORKDIR /app

COPY --from=build /app/dist ./dist
COPY --from=build /app/node_modules ./node_modules
COPY ./config ./config
COPY ./tools ./tools
COPY ./databases ./databases

CMD node dist/server/index.js
